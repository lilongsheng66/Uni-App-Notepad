# Uni-App记事本

#### 介绍
基于uniapp、uviewUI、colorUI实现的记事本，适用于微信小程序。后端基于node.js，已放置在云服务器上。


#### 安装教程

1.  下载之后直接导入HbuilderX，可直接使用
2.  如若想改成自己的本地项目，需要修改code/routes/conn.js配置文件,同时需要注释cashbook/main.js中的全局代理，修改mainifest.json中的跨域配置。

#### 使用说明

1.  HBuilderx中可选择运行到浏览器或者在微信小程序中启动（需要安装微信开发者工具，同时需要申请APPID）。
2.  包括用户登录、注册，新增备忘、删除、修改备忘等功能，注意长按删除需要在微信开发者工具中才可以运行，或者使用真机调式。

#### 运行界面

![1649475705942](C:\Users\LILONG~1\AppData\Local\Temp\1649475705942.png)

![1649475759611](C:\Users\LILONG~1\AppData\Local\Temp\1649475759611.png)

![1649475789602](C:\Users\LILONG~1\AppData\Local\Temp\1649475789602.png)


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
