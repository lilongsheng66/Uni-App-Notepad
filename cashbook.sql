/*
 Navicat Premium Data Transfer

 Source Server         : itcast
 Source Server Type    : MySQL
 Source Server Version : 80011
 Source Host           : localhost:3306
 Source Schema         : cashbook

 Target Server Type    : MySQL
 Target Server Version : 80011
 File Encoding         : 65001

 Date: 18/10/2022 17:46:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for memo
-- ----------------------------
DROP TABLE IF EXISTS `memo`;
CREATE TABLE `memo`  (
  `memoId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `date` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`memoId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of memo
-- ----------------------------
INSERT INTO `memo` VALUES (6, 1, NULL, '你好！测试！', '2022-04-07 00:00:00');
INSERT INTO `memo` VALUES (7, 1, NULL, '我是李龙生！', '2022-04-07 00:00:00');
INSERT INTO `memo` VALUES (8, 2, NULL, '我是段惠慧！', '2022-04-07 14:53:39');
INSERT INTO `memo` VALUES (13, 1, '学习', '学习java', '2022-04-07 00:00:00');
INSERT INTO `memo` VALUES (14, 1, '今日计划', '打游戏，学习Java！', '2022-04-07 00:00:00');
INSERT INTO `memo` VALUES (15, 35, 'AA', '滴滴', '2022-05-15 12:27:08');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userPwd` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, '段惠慧', '123456', '13508471445');
INSERT INTO `users` VALUES (2, 'guest', '123456', '13647497007');
INSERT INTO `users` VALUES (3, 'admin', '123456', '13659898945');
INSERT INTO `users` VALUES (34, '李硕', '123456', '13508471447');
INSERT INTO `users` VALUES (35, 'admin', '123456', '15974229766');

-- ----------------------------
-- Table structure for wechatuser
-- ----------------------------
DROP TABLE IF EXISTS `wechatuser`;
CREATE TABLE `wechatuser`  (
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nickName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gender` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wechatuser
-- ----------------------------
INSERT INTO `wechatuser` VALUES ('op9Ul5HUNjC3bnzYoZ1UKWrBQSZU', '优秀的李', 'https://thirdwx.qlogo.cn/mmopen/vi_32/xQ8bZzDqfx235WRpT4FibHqCHQQmwTGC2kyBiaqeTf4BeFVLVdfFl6byOh1M1g5FTpx7cCpia5Kzuicd1BCUYRRoNw/132', '0');

SET FOREIGN_KEY_CHECKS = 1;
